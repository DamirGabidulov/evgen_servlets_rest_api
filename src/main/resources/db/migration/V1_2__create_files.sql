create table if not exists files (
    id serial primary key,
    file_path varchar (255),
    name varchar (255)
)

