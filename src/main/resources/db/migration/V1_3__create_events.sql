create table if not exists events(
    id serial primary key,
    file_id integer not null,
    foreign key (file_id) references files(id),
    user_id integer not null,
    foreign key (user_id) references users(id)
)