package repository.impl;

import model.User;
import org.hibernate.Session;
import org.hibernate.query.Query;
import repository.UserRepository;

import java.util.List;
import java.util.Optional;

import static util.HibernateUtils.getSession;

public class UserRepositoryImpl implements UserRepository {

    private static final String SQL_FIND_ALL = "from User user order by user.id";
    private static final String SQL_GET_BY_ID = "select user from User user left join fetch user.events where user.id = :id";

    @Override
    public List<User> findAll() {
        try(Session session = getSession()){
            Query<User> userQuery = session.createQuery(SQL_FIND_ALL, User.class);
            return userQuery.getResultList();
        }
    }

    @Override
    public Optional<User> getById(Integer id) {
        try (Session session = getSession()){
            Query<User> userQuery = session.createQuery(SQL_GET_BY_ID, User.class);
            userQuery.setParameter("id", id);
            return userQuery.getResultList().isEmpty() ? Optional.empty() : Optional.of(userQuery.getSingleResult());
        }
    }

    @Override
    public User save(User user) {
        try (Session session = getSession()) {
            session.getTransaction().begin();
            session.persist(user);
            User savedUser = session.load(User.class, user.getId());
            session.getTransaction().commit();
            return savedUser;
        }
    }

    //TODO: апдейт только на имя юзера, при изменении ивента логику прописывать выше уровнем, если в этом вообще есть смысл
    @Override
    public User update(User user) {
        try(Session session = getSession()){
            session.getTransaction().begin();
            session.update(user);
            User updatedUser = session.load(User.class, user.getId());
            session.getTransaction().commit();
            return updatedUser;
        }
    }

    @Override
    public void deleteById(Integer id) {
        try (Session session = getSession()){
            session.getTransaction().begin();
            User expectedUser = session.load(User.class, id);
            session.delete(expectedUser);
            session.getTransaction().commit();
        }
    }
}
