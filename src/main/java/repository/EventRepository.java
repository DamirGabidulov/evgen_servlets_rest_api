package repository;

import model.Event;

import java.util.List;
import java.util.Optional;

public interface EventRepository extends GenericRepository<Event,Integer> {
    @Override
    List<Event> findAll();

    @Override
    Optional<Event> getById(Integer id);

    @Override
    Event save(Event event);

    @Override
    Event update(Event event);

    @Override
    void deleteById(Integer id);
}
