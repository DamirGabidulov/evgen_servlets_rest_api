package dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import model.File;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileDto {

    private Integer id;
    private String name;
    private String filePath;

    public static FileDto from(File file){
        return FileDto.builder()
                .id(file.getId())
                .name(file.getName())
                .filePath(file.getFilePath())
                .build();
    }

    public static List<FileDto> from(List<File> files){
        return files.stream().map(FileDto::from).collect(Collectors.toList());
    }
}
