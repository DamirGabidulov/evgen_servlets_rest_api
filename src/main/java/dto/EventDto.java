package dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import model.Event;
import model.File;
import model.User;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EventDto {
    private Integer id;
    @JsonIgnore
    private User user;
    private File file;

    public static EventDto from (Event event){
        return EventDto.builder()
                .id(event.getId())
                .user(event.getUser())
                .file(event.getFile())
                .build();
    }

    public static List<EventDto> from(List<Event> events){
        return events.stream().map(EventDto::from).collect(Collectors.toList());
    }
}
