package dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import model.Event;
import model.User;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
    private Integer id;
    private String name;
    @JsonIgnore
    private List<Event> events;

    public static UserDto from (User user){
        return UserDto.builder()
                .id(user.getId())
                .name(user.getName())
                .events(user.getEvents())
                .build();
    }

    public static List<UserDto> from (List<User> users){
        return users.stream().map(UserDto::from).collect(Collectors.toList());
    }
}
