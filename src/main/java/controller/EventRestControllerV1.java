package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import dto.EventDto;
import dto.UserDto;
import model.Event;
import model.User;
import service.EventService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = {"/api/v1/events", "/api/v1/events/*"})
public class EventRestControllerV1 extends HttpServlet {

    private final UserService userService = new UserService();
    private final EventService eventService = new EventService();
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (userService.isUserExist(request)) {
            UserDto user = userService.getById(Integer.parseInt(request.getHeader("userId")));
            String eventId = request.getPathInfo();
            if (eventId == null){
                List<Event> events = user.getEvents();
                String responseBody = objectMapper.writeValueAsString(events);
                response.setContentType("application/json");
                response.setStatus(200);
                response.getWriter().println(responseBody);
            } else {
                try {
                    EventDto expectedEvent = eventService.getEvent(user, eventId);
                    String responseBody = objectMapper.writeValueAsString(expectedEvent);
                    response.setContentType("application/json");
                    response.setStatus(200);
                    response.getWriter().println(responseBody);
                }catch (IllegalArgumentException e){
                    notificationEventBadRequest(response, e.getMessage());
                }
            }
        } else {
            notificationUserBadRequest(response);
        }
    }

    private void notificationUserBadRequest(HttpServletResponse response) throws IOException {
        response.setStatus(400);
        response.getWriter().println("Don't get user with this id");
    }

    private void notificationEventBadRequest(HttpServletResponse response, String message) throws IOException {
        response.setStatus(400);
        response.getWriter().println(message);
    }
}
