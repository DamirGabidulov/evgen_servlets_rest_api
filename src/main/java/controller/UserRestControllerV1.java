package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import dto.UserDto;
import model.User;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public class UserRestControllerV1 extends HttpServlet {

    private final UserService userService = new UserService();
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder builder = getFromBody(request);
        UserDto userDto = objectMapper.readValue(builder.toString(), UserDto.class);
        UserDto updatedUser = userService.update(userDto);
        String responseBody = objectMapper.writeValueAsString(updatedUser);
        response.setContentType("application/json");
        response.setStatus(200);
        response.getWriter().println(responseBody);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // get ID from request URL DELETE: /api/v1/users/1
        String id = request.getPathInfo().substring(1);
        userService.deleteById(Integer.parseInt(id));
        response.setStatus(200);
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getPathInfo();
        if(id == null) {
            List<UserDto> users = userService.findAll();
            response.setContentType("application/json");
            //сконвертирует список объектов в json
            String json = objectMapper.writeValueAsString(users);
            response.setStatus(200);
            //записываем в ответ json
            response.getWriter().println(json);
        } else {
            id = request.getPathInfo().substring(1);
            UserDto user = userService.getById(Integer.parseInt(id));
            response.setContentType("application/json");
            String json = objectMapper.writeValueAsString(user);
            response.setStatus(200);
            response.getWriter().println(json);
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder builder = getFromBody(request);
        UserDto userDto = objectMapper.readValue(builder.toString(), UserDto.class);
        UserDto savedUser = userService.save(userDto);
        String responseBody = objectMapper.writeValueAsString(savedUser);
        response.setContentType("application/json");
        response.setStatus(201);
        response.getWriter().println(responseBody);
    }

    private StringBuilder getFromBody(HttpServletRequest request) throws IOException {
        BufferedReader bufferedReader = request.getReader();
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            builder.append(line);
        }
        return builder;
    }
}
