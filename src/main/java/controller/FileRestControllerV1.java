package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import dto.FileDto;
import dto.UserDto;
import model.Event;
import model.File;
import model.User;
import service.FileService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;


@WebServlet(urlPatterns = {"/api/v1/files", "/api/v1/files/*"})
@MultipartConfig
public class FileRestControllerV1 extends HttpServlet {

    private final FileService fileService = new FileService();
    private final UserService userService = new UserService();
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (userService.isUserExist(request)) {
            UserDto user = userService.getById(Integer.parseInt(request.getHeader("userId")));
            String fileId = request.getPathInfo();

            if (fileId == null) {
                List<File> files = user.getEvents().stream().map(Event::getFile).collect(Collectors.toList());
                String responseBody = objectMapper.writeValueAsString(files);
                response.setContentType("application/json");
                response.setStatus(200);
                response.getWriter().println(responseBody);
            } else {
                Integer id = Integer.parseInt(fileId.substring(1));
                if (fileService.isFileExist(user, id)) {
                    FileDto fileDto = fileService.getById(id);
                    String contentType = Files.probeContentType(Paths.get(fileDto.getFilePath(), fileDto.getName()));
                    response.setContentType(contentType.isEmpty() ? "application/octet-stream" : contentType);
                    //inline - отображение в браузере, attachment - скачивание
                    response.setHeader("Content-disposition", "inline; filename=\"" + fileDto.getName() + "\"");
                    response.setContentLengthLong(Files.size(Paths.get(fileDto.getFilePath(), fileDto.getName())));
                    response.setStatus(200);
                    fileService.printFile(response, fileDto);
                    response.flushBuffer();
                } else {
                    notificationFileBadRequest(response);
                }
            }
        } else {
            notificationUserBadRequest(response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (userService.isUserExist(request)) {
            FileDto fileDto = fileService.save(request);
            String responseBody = objectMapper.writeValueAsString(fileDto);
            response.setContentType("application/json");
            response.setStatus(201);
            response.getWriter().println(responseBody);
        } else {
            notificationUserBadRequest(response);
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (userService.isUserExist(request)) {
            FileDto updatedFileDto = fileService.update(request, objectMapper);
            String responseBody = objectMapper.writeValueAsString(updatedFileDto);
            response.setContentType("application/json");
            response.setStatus(200);
            response.getWriter().println(responseBody);
        } else {
            notificationUserBadRequest(response);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (userService.isUserExist(request)) {
            UserDto user = userService.getById(Integer.parseInt(request.getHeader("userId")));
            String fileId = request.getPathInfo();
            if (fileId == null) {
                response.setStatus(400);
                response.getWriter().println("Please write id in url");
            } else {
                Integer id = Integer.parseInt(fileId.substring(1));
                if (fileService.isFileExist(user, id)) {
                    fileService.deleteById(request, id);
                    response.setStatus(200);
                } else {
                    notificationFileBadRequest(response);
                }
            }
        } else {
            notificationUserBadRequest(response);
        }
    }


    private void notificationUserBadRequest(HttpServletResponse response) throws IOException {
        response.setStatus(400);
        response.getWriter().println("Don't get user with this id");
    }

    private void notificationFileBadRequest(HttpServletResponse response) throws IOException {
        response.setStatus(400);
        response.getWriter().println("Don't get file with this id");
    }

}

