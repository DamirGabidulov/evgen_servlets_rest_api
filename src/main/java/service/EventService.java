package service;

import dto.EventDto;
import dto.UserDto;
import model.Event;
import model.User;
import repository.EventRepository;
import repository.impl.EventRepositoryImpl;

import java.util.List;

import static dto.EventDto.from;

public class EventService {

    private EventRepository eventRepository;

    public EventService() {
        this.eventRepository = new EventRepositoryImpl();
    }

    public void setEventRepository(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public List<Event> findAll(){
        return eventRepository.findAll();
    }

    public EventDto save(EventDto eventDto){
        Event event = Event.builder()
                .id(eventDto.getId())
                .user(eventDto.getUser())
                .file(eventDto.getFile())
                .build();

        return from(eventRepository.save(event));
    }

    public void deleteById(Integer id){
        eventRepository.deleteById(id);
    }

    public EventDto getEvent(UserDto userDto, String eventId) {
        Integer id = Integer.parseInt(eventId.substring(1));
        return from(userDto.getEvents().stream()
                .filter(event -> event.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Can't find event with this id")));
    }
}
