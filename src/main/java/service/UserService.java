package service;

import dto.UserDto;
import model.User;
import repository.UserRepository;
import repository.impl.UserRepositoryImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static dto.UserDto.from;

public class UserService {

    private UserRepository userRepository;

    public UserService() {
        this.userRepository = new UserRepositoryImpl();
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserDto> findAll(){
        return from(userRepository.findAll());
    }

    public UserDto save(UserDto userDto){
        User user = User.builder()
                .name(userDto.getName())
                .events(userDto.getEvents())
                .build();
        return from(userRepository.save(user));
    }

    public UserDto getById(Integer id){
        return from(userRepository.getById(id).orElseThrow(() -> new IllegalArgumentException("Wrong id")));
    }

    public UserDto update(UserDto userDto){
        User user = User.builder()
                .id(userDto.getId())
                .name(userDto.getName())
                .events(userDto.getEvents())
                .build();
        return from(userRepository.update(user));
    }

    public void deleteById(Integer id){
        userRepository.deleteById(id);
    }

    public boolean isUserExist(HttpServletRequest request) {
        String userId = request.getHeader("userId");
        return userId != null && findAll().stream().map(UserDto::getId)
                .anyMatch(id -> id.equals(Integer.parseInt(userId)));
    }
}
