package service;

import dto.FileDto;
import model.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileCopyService {
    public void copyFile(Part filePart, String storagePath, String fileName) throws IOException {
        Files.copy(filePart.getInputStream(), Paths.get(storagePath, fileName));
    }

    public static void moveFile(File newFile, FileDto previousFileDto) throws IOException {
        Path previousFilePath = Paths.get(previousFileDto.getFilePath(), previousFileDto.getName());
        Files.move(previousFilePath, previousFilePath.resolveSibling(newFile.getName()));
    }

    public static StringBuilder getFromBody(HttpServletRequest request) throws IOException {
        BufferedReader bufferedReader = request.getReader();
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            builder.append(line);
        }
        return builder;
    }
}
