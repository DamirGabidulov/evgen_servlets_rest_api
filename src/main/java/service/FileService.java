package service;

import com.fasterxml.jackson.databind.ObjectMapper;
import dto.EventDto;
import dto.FileDto;
import dto.UserDto;
import model.Event;
import model.File;
import model.User;
import repository.FileRepository;
import repository.impl.FileRepositoryImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

import static dto.UserDto.from;
import static dto.FileDto.from;

public class FileService {

    private final FileRepository fileRepository;
    private final UserService userService;
    private final EventService eventService;

    private final FileCopyService fileCopyService;

    public FileService() {
        this.fileRepository = new FileRepositoryImpl();
        this.userService = new UserService();
        this.eventService = new EventService();
        this.fileCopyService = new FileCopyService();
    }

    public FileService(FileRepository fileRepository, UserService userService, EventService eventService, FileCopyService fileCopyService) {
        this.fileRepository = fileRepository;
        this.userService = userService;
        this.eventService = eventService;
        this.fileCopyService = fileCopyService;
    }

    public FileDto save(HttpServletRequest request) {
        try {
            Part filePart = request.getPart("file");
            Integer userId = Integer.parseInt(request.getHeader("userId"));
            UserDto userDto = userService.getById(userId);
            User user = User.builder()
                    .id(userDto.getId())
                    .name(userDto.getName())
                    .events(userDto.getEvents())
                    .build();

            Properties properties = new Properties();
            properties.load(getClass().getClassLoader().getResourceAsStream("application.properties"));
            String storagePath = properties.getProperty("storage.path");
            String fileName = filePart.getSubmittedFileName();

            File file = new File();
            file.setFilePath(storagePath);
            file.setName(fileName);
            fileCopyService.copyFile(filePart, storagePath, fileName);
            File createdFile = fileRepository.save(file);


            EventDto event = new EventDto();
            event.setFile(createdFile);
            event.setUser(user);
            eventService.save(event);
            return from(createdFile);
        } catch (IOException | ServletException e) {
            throw new RuntimeException(e);
        }
    }




    public void printFile(HttpServletResponse response, FileDto fileDto) {
        try {
            Files.copy(Paths.get(fileDto.getFilePath(), fileDto.getName()), response.getOutputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public List<FileDto> findAll() {
        return from(fileRepository.findAll());
    }

    public FileDto getById(Integer id) {
        return from(fileRepository.getById(id).orElseThrow(() -> new IllegalArgumentException("Wrong id")));
    }

    public FileDto update(HttpServletRequest request, ObjectMapper objectMapper) throws IOException {
        StringBuilder builder = FileCopyService.getFromBody(request);
        File newFile = objectMapper.readValue(builder.toString(), File.class);
        FileDto previousFileDto = getById(newFile.getId());
        FileCopyService.moveFile(newFile, previousFileDto);
        return from(fileRepository.update(newFile));
    }



    public void deleteById(HttpServletRequest request, Integer fileId) throws IOException {
        FileDto fileDto = getById(fileId);
        Event event = userService.getById(1).getEvents().stream()
                .filter(userEvent -> userEvent.getFile().getId().equals(fileId))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Can't find event and file with this id"));
        eventService.deleteById(event.getId());
        fileRepository.deleteById(fileId);
        Files.delete(Paths.get(fileDto.getFilePath(), fileDto.getName()));
    }



    public boolean isFileExist(UserDto user, Integer id) {
        return user.getEvents().stream()
                .map(Event::getFile)
                .anyMatch(file -> file.getId().equals(id));
    }
}
