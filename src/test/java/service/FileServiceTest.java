package service;

import com.fasterxml.jackson.databind.ObjectMapper;
import dto.UserDto;
import model.File;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.FileRepository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import static dto.FileDto.from;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static service.TestUtils.*;

@ExtendWith(MockitoExtension.class)
class FileServiceTest {

    @Mock
    FileRepository fileRepository;
    @Mock
    UserService userService;
    @Mock
    EventService eventService;

    @Mock
    FileCopyService fileCopyService;
    @Mock
    HttpServletRequest httpServletRequest;
    @Mock
    Part filePart;

    @Mock
    ObjectMapper objectMapper;
    @InjectMocks
    FileService fileService;
    public static final String FILE_NAME = "bill.png";
    public static final String OLD_FILE_NAME = "old.png";
    String json = "{\"id\":5, \"name\":\"old.png\", \"filePath\":\"C:\\Users\\\"}";

    @BeforeEach
    void setUp() throws ServletException, IOException {

        Mockito.lenient().when(fileRepository.findAll()).thenReturn(getFiles());
        Mockito.lenient().when(fileRepository.getById(1)).thenReturn(Optional.of(getFiles().get(0)));
        Mockito.lenient().when(fileRepository.save(File.builder().name(FILE_NAME).filePath(getStoragePath()).build())).thenReturn(getFileWithId());
        Mockito.lenient().when(httpServletRequest.getHeader("userId")).thenReturn("1");
        Mockito.lenient().when(userService.getById(1)).thenReturn(UserDto.from(getUsers().get(0)));
        Mockito.lenient().when(httpServletRequest.getPart("file")).thenReturn(filePart);
        Mockito.lenient().when(filePart.getSubmittedFileName()).thenReturn(FILE_NAME);
        Mockito.lenient().doNothing().when(fileCopyService).copyFile(filePart, getStoragePath(), FILE_NAME);
        Mockito.lenient().when(fileRepository.update(File.builder().id(5).name(FILE_NAME).filePath(getStoragePath()).build())).thenReturn(getFileWithId());
        Mockito.lenient().when(objectMapper.readValue(json, File.class)).thenReturn(getFileWithId());
        Mockito.lenient().when(httpServletRequest.getReader()).thenReturn(
                new BufferedReader(new StringReader(json)));
        Mockito.lenient().when(fileRepository.getById(5)).thenReturn(Optional.of(File.builder().id(5).name(OLD_FILE_NAME).filePath(getStoragePath()).build()));

        fileService = new FileService(fileRepository, userService, eventService, fileCopyService);

    }

    @Test
    void findAll() {
        assertEquals(fromFileToDto(getFiles()), fileService.findAll());
    }

    @ParameterizedTest
    @ValueSource(ints = 1)
    void getById(Integer id) {
        assertEquals(from(getFiles().get(0)), fileService.getById(id));
    }

    @Test
    void save() {
        assertEquals(from(getFileWithId()), fileService.save(httpServletRequest));
    }

    @Test
    void update(){
        try {
            assertEquals(from(getFileWithId()), fileService.update(httpServletRequest, objectMapper));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @AfterAll
    static void rename(){
        Path previousFilePath = Paths.get(getStoragePath(), FILE_NAME);
        try {
            Files.move(previousFilePath, previousFilePath.resolveSibling(OLD_FILE_NAME));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}