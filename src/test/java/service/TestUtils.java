package service;

import dto.FileDto;
import model.Event;
import model.File;
import model.User;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static service.FileServiceTest.FILE_NAME;

public class TestUtils {



    public static String getStoragePath(){
        try {
            Properties properties = new Properties();
            properties.load(TestUtils.class.getClassLoader().getResourceAsStream("application.properties"));
            return properties.getProperty("storage.path");
        } catch (IOException e) {
            throw new IllegalArgumentException("can't find file application.properties");
        }
    }

    public static List<File> getFiles(){
        List<File> files = new LinkedList<>();
        files.add(new File(1, "first.png", "/storage/folder 1"));
        files.add(new File(2, "second.png", "/storage/folder 2"));
        files.add(new File(1, "third.png", "/storage/folder 3"));
        files.add(new File(1, "fourth.png", "/storage/folder 4"));
        return files;
    }

    public static List<FileDto> fromFileToDto(List<File> files){
        return files.stream().map(FileDto::from).collect(Collectors.toList());
    }


    public static File getFileWithId(){
        return File.builder().id(5).name(FILE_NAME).filePath(getStoragePath()).build();
    }

    public static List<User> getUsers(){
        List<User> users = new LinkedList<>();
        users.add(new User(1, "Tarantino", Arrays.asList(getEvents().get(0), getEvents().get(1))));
        users.add(new User(2, "Nolan", Arrays.asList(getEvents().get(2), getEvents().get(3))));
        return users;
    }

    public static List<Event> getEvents(){
        List<Event> events = new ArrayList<>();
        events.add(new Event(1, new User(1, "Tarantino", null), getFiles().get(0)));
        events.add(new Event(2, new User(1, "Tarantino", null), getFiles().get(1)));
        events.add(new Event(3, new User(2, "Nolan", null), getFiles().get(2)));
        events.add(new Event(4, new User(2, "Nolan", null), getFiles().get(3)));
        return events;
    }


}
